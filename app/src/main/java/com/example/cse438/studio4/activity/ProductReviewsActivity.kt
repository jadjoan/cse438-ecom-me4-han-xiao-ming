package com.example.cse438.studio4.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.MenuItem
import com.example.cse438.studio4.R
import com.example.cse438.studio4.adapter.ReviewAdapter
import com.example.cse438.studio4.model.Review
import com.google.firebase.Timestamp
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_product_reviews.*

class ProductReviewsActivity: AppCompatActivity() {
    private var reviewList = ArrayList<Review>()
    private lateinit var adapter: ReviewAdapter

    private lateinit var itemId: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_reviews)

        this.itemId = intent.getStringExtra("ItemId")


        adapter = ReviewAdapter(this, reviewList)
        review_items_list.adapter = adapter
    }

    override fun onStart() {
        super.onStart()

        val db = FirebaseFirestore.getInstance()

        // TODO: Finish implementing with given pseudocode
//        db.collection("items").document(itemId).get().addOnCompleteListener { task ->
//            if(!task.isSuccessful){
//                Log.e("TEXT", "view reviews failure")
//                return@addOnCompleteListener
//            }
//            var localvar = task.result as MutableMap<String, Any>?
//            var reviews = localvar?.get("reviews") as? ArrayList<HashMap<String,Any>>
//            if (reviews != null && reviews.size > 0) {
//                for (rev in reviews) {
//                    reviewList.add(
//                        Review(
//                            rev["body"] as String,
//                            rev["isAnonymous"] as Boolean,
//                            rev["userId"] as String,
//                            rev["username"] as String,
//                            rev["date"] as Timestamp
//                        )
//                    )
//                }
//                adapter.notifyDataSetChanged()
//            }
//        }
        db.collection("items").document(itemId).addSnapshotListener { documentSnapshot, firebaseFirestoreException ->
            if (firebaseFirestoreException != null) {
                Log.e("SNAPSHOT", "Listen failed: $firebaseFirestoreException")
                return@addSnapshotListener
            }

            val data: MutableMap<String, Any>? = documentSnapshot?.data
            val reviews = data?.get("reviews") as? ArrayList<HashMap<String, Any>>

            if (reviews != null) {
                for (review in reviews) {
                    val rev = Review(
                        review["body"] as String,
                        review["isAnonymous"] as Boolean,
                        review["userId"] as String,
                        review["username"] as String,
                        review["date"] as Timestamp
                    )
                    reviewList.add(rev)
                }

                runOnUiThread {
                    adapter.notifyDataSetChanged() // not seeing ui update now...
                }
            }
        }

        reviewList.add(Review())
        adapter.notifyDataSetChanged()
    }

    override fun onBackPressed() {
        this.finish()
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}