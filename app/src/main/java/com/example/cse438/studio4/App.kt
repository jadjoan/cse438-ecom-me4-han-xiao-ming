package com.example.cse438.studio4

import android.app.ActionBar
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.example.cse438.studio4.model.Review
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.dialog_add_review.*

class App {
    companion object {
        var firebaseAuth: FirebaseAuth? = null

        fun openReviewDialog(context: Context, itemId: String) {
            val dialog = Dialog(context)

            dialog.setContentView(R.layout.dialog_add_review)

            val window = dialog.window
            window?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

            dialog.findViewById<Button>(R.id.close).setOnClickListener {
                dialog.dismiss()
            }

            dialog.findViewById<Button>(R.id.submit).setOnClickListener {
                // TODO: Finish implementing using given pseudocode
//                //xjq begin
//                var body = R.id.body.toString()
//                var isAnonymous = R.id.anonymous.toString().toBoolean()
//                lateinit var localuserId: String
//
//                if(App.firebaseAuth?.currentUser!= null && body.toString().isNotEmpty()){
//                    localuserId = firebaseAuth?.currentUser!!.uid
//                    val db = FirebaseFirestore.getInstance()
//                    //firebase call, first collection, then document
//                    db.collection("items").document(itemId).get().addOnCompleteListener { task ->
//                        if(task.isSuccessful){
//                            var tempreview = task.result?.get("reviews") as? ArrayList<Review>
//
//                            if(tempreview == null){
//                                tempreview = ArrayList<Review>()
//                            }
//
//                            db.collection("users").document(localuserId).get().addOnCompleteListener{task2 ->
//                                if(task2.isSuccessful && task2.result != null){
//                                    var userdata = task2.result
//                                    var userreview = userdata!!.get("reviews") as? ArrayList<String>
//                                    if(userreview == null){
//                                        userreview = ArrayList<String>()
//                                    }
//                                    if(!userreview.contains(itemId)){
//                                        userreview.add(itemId)
//                                    }
//                                    //Add a new Review object to the reviews Arraylist
//                                    tempreview.add(
//                                        Review(
//                                            body,
//                                            isAnonymous,
//                                            localuserId,
//                                            userdata.get("username") as String,
//                                            Timestamp.now()
//                                        )
//                                    )
//                                    //create a HashMap
//                                    var reviewinfo = HashMap<String, Any>()
//                                    reviewinfo["reviews"] = tempreview.toList()
//                                    db.collection("items").document(itemId)
//                                        .set(reviewinfo)
//                                        .addOnSuccessListener { Log.d("TEXT", "add items successfully!") }
//                                        .addOnFailureListener { e -> Log.w("TEXT", "Error adding items", e) }
//
//                                    //create another hashmap
//                                    var userviewinfo = HashMap<String, Any?>()
//                                    userviewinfo["first_name"] = userdata?.get("first_name")
//                                    userviewinfo["last_name"] = userdata?.get("last_name")
//                                    userviewinfo["email"] = userdata?.get("email")
//                                    userviewinfo["username"] = userdata?.get("username")
//                                    //include reviews made by the user
//                                    userviewinfo["reviews"] = tempreview.toList()
//                                    db.collection("users").document(localuserId)
//                                        .set(userviewinfo)
//                                        .addOnSuccessListener { Log.d("TEXT", "add users success") }
//                                        .addOnFailureListener { e -> Log.w("TEXT", "Error adding users", e) }
//                                }
//                            }
//                        }
//                    }
//                    Toast.makeText(context," Review Submitted!",Toast.LENGTH_SHORT).show()
//                    dialog.dismiss()
//                }
//                else{
//                    Toast.makeText(context," Unable to submit review!",Toast.LENGTH_SHORT).show()
//                }
//                //xjq end
                val reviewText = dialog.findViewById<EditText>(R.id.body).text.toString()
                val userId = firebaseAuth?.currentUser?.uid
                val isAnonymous = dialog.anonymous.isChecked

                if (userId != null && reviewText != "") {
                    val db = FirebaseFirestore.getInstance()

                    db.collection("items").document(itemId).get().addOnCompleteListener { it1 ->
                        if (it1.isSuccessful) {
                            var data = it1.result?.get("reviews") as? ArrayList<Review>

                            if (data == null) {
                                data = ArrayList()
                            }

                            db.collection("users").document(firebaseAuth!!.currentUser!!.uid).get().addOnCompleteListener { it2 ->
                                if (it2.isSuccessful) {
                                    val userData = it2.result!!
                                    var data2 = userData.get("reviews") as? ArrayList<String>

                                    if (data2 == null) {
                                        data2 = ArrayList()
                                    }

                                    if (!data2.contains(itemId)) {
                                        data2.add(itemId)
                                    }

                                    data.add(
                                        Review(
                                            reviewText,
                                            isAnonymous,
                                            userId,
                                            userData.get("username") as String,
                                            Timestamp.now()
                                        )
                                    )

                                    val hashMap: HashMap<String, List<Review>> = HashMap()
                                    hashMap["reviews"] = data.toList()

                                    val map = hashMapOf(Pair("reviews", data.toList()))

                                    db.collection("items").document(itemId).set(map)

                                    val map2 = hashMapOf(
                                        Pair("first_name", userData.get("first_name")),
                                        Pair("last_name", userData.get("last_name")),
                                        Pair("email", userData.get("email")),
                                        Pair("username", userData.get("username")),
                                        Pair("reviews", data2.toList())
                                    )

                                    db.collection("users").document(firebaseAuth!!.currentUser!!.uid).set(map2)
                                }
                            }
                        }
                    }

                    Toast.makeText(context, "Review submitted!", Toast.LENGTH_SHORT).show()

                    dialog.dismiss()
                }
                else {
                    Toast.makeText(context, "Unable to submit review", Toast.LENGTH_SHORT).show()
                }
            }
            dialog.show()
        }
    }
}